function out = getbiggestobject(BW)
% out = getbiggestobject(img)
% returns biggest object in the binary image

    CC = bwconncomp(BW);
    numPixels = cellfun(@numel,CC.PixelIdxList);
    [~,idx] = max(numPixels);
    out = false(size(BW));
    out(CC.PixelIdxList{idx}) = 1;