function cplxfilter(file1,file2,file3)
%
% cplxfilter('nifti/mag1.nii.gz','nifti/pha1.nii.gz','nifti/abs1.nii.gz')
%
disp('In cplxfilter')
disp (file1)
disp (file2)
disp (file3)
imgmag = load_untouch_nii(file1);
imph = load_untouch_nii(file2);
imcpl=(imgmag.img).*exp((1i).*(imph.img));
%c = fftshift(ifftn(fftshift(imcpl)));
%c = flipdim(flipdim(flipdim(c,1),2),3);
f = fspecial3('gaussian', [3 3 3], 1); %/sqrt(2));
%compleximg = complex(imfilter(real(c), f), imfilter(imag(c), f));
compleximg = complex(imfilter(real(imcpl), f), imfilter(imag(imcpl), f));
nii=imgmag;
nii.img=abs(compleximg);
save_untouch_nii(nii,file3);

