#!/usr/bin/env bash
## average_scans.sh
#
# - (C) 2014 Michael Eager, Monash Biomedical Imaging
# 

if [ $# -lt 3 ]; then
    echo "average_scans.sh must have at least 3 arguments: "
    echo "average_scans.sh output.nii img1 img2 [img3...imgN] "
    
    exit 1
fi
BionicVision_scripts=$(dirname $0)
output_file=$1

[ -d nifti ] && rm -rf nifti
mkdir nifti
[ -d ants ] && rm -rf ants
mkdir ants

if [ ! -d $(dirname $1) ]; then
    echo " Output file directory does not exist."
    mkdir $(dirname $1)
fi  

echo " Running dicom->nifti"

declare -a scans=( $@ )
len=${#scans[@]}
inputscans=${scans[@]:1:$len}
len=${#inputscans[*]}
scanidx=0

for scan in "${inputscans[@]}"; do
    echo "Converting " $scan " to nifti "
    imgname=nifti/scan${scanidx}
    mrconvert -info -datatype float32 ${scan} ${imgname}.nii.gz 
    echo " Running make_mask"
    matlab -nodesktop -nosplash -r "addpath(genpath('$BionicVision_scripts'));make_mask('nifti/scan${scanidx}.nii.gz');quit" 

    mrtransform ${imgname}.nii.gz -template ${imgname}.nii.gz -upsample 2 ${imgname}_dbl_res.nii.gz

    if (( scanidx == 0 ))
	cp ${imgname}_dbl_res.nii.gz nifti/tmp_dbl_res.nii.gz
    else
	echo " Running ants"
	(cd ants && \
	    ants 3 -m CC[scan0_for_reg.nii.gz,scan${scanidx}_for_reg.nii.gz,1,2] -o ${scanidx}to0 -i 0 --rigid-affine true && \
	    cd ..)
	echo " Warping scan to first scan "
	WarpImageMultiTransform 3 ${imgname}_dbl_res.nii.gz ${imgname}_in_img0_dbl_res.nii.gz -R nifti/scan0_dbl_res.nii.gz --use-BSpline ants/${scanidx}to0Affine.txt
	cp nifti/tmp_dbl_res.nii.gz nifti/tmp1_dbl_res.nii.gz && rm -f nifti/tmp_dbl_res.nii.gz
	fslmaths nifti/tmp1_dbl_res.nii.gz -add ${imgname}_in_img0_dbl_res.nii.gz nifti/tmp_dbl_res.nii.gz
    fi
    ((++scanidx))
done
echo " Combining scans "
fslmaths nifti/tmp_dbl_res.nii.gz -div $len $output_file
echo " Cleaning up."
rm -f nifti/*_dbl_res.nii.gz

