function make_mask(filename)


  if ~exist(filename,'file')
    error('make_mask','Unable to find input file.')
  end

orignii = load_untouch_nii(filename);
mag = orignii.img;

snr = mag./stdfilt(mag);
snr(isnan(snr)) = 0;
snr(isinf(snr)) = 0;

tmp = normalise(imdilate(snr,ball(3)));
mask = tmp > graythresh(tmp);
mask = getbiggestobject(mask);
mask = imclose(mask,ball(5));
mask = imdilate(mask,ball(4));

mag_masked = mag;
mag_masked(~mask) = nan;

%imagesc3(tmp,'mask',mask)

%% SAVE MAG AND MASK

minX = find(sum(sum(mask,2),3),1,'first');
maxX = find(sum(sum(mask,2),3),1,'last');

minY = find(sum(sum(mask,1),3),1,'first');
maxY = find(sum(sum(mask,1),3),1,'last');

nii = orignii;
hdr = orignii.hdr;

nii.img = mag_masked(minX:maxX, minY:maxY, :);

nii.hdr = orignii.hdr;
nii.hdr.dime.dim(2:4) = size(nii.img);

affine = cat(1, hdr.hist.srow_x, hdr.hist.srow_y, hdr.hist.srow_z);
newtranslation = affine*[minX-1; minY-1; 0; 1];
nii.hdr.hist.qoffset_x = newtranslation(1);
nii.hdr.hist.qoffset_y = newtranslation(2);
nii.hdr.hist.qoffset_z = newtranslation(3);
nii.hdr.hist.srow_x(4) = newtranslation(1);
nii.hdr.hist.srow_y(4) = newtranslation(2);
nii.hdr.hist.srow_z(4) = newtranslation(3);

basename=regexpi(filename,'/(\w*).','tokens');
save_untouch_nii(nii, sprintf('ants/%s_for_reg.nii.gz',char(basename{1})))
