#!/usr/bin/env bash

# - Michael Eager
# Documenting pipeline in Bionic Vision Group's project MRH-002

if [ ! -d dicom_series ]; then
    echo " This script must be run in data directory parent to dicom_series."
    exit 1
fi  

mkdir nifti
mkdir ants
rm -f ants/2to1Affine.txt ants/3to1Affine.txt
echo " Cleaning any previous runs."
rm -f nifti/*.nii.gz ants/*.nii.gz


echo " Running dicom->nifti"
# /Volumes/AmandaProjects/Projects/MRH002_BionicVision/scripts/prepare_surfscans.sh
declare -a surfscans=(`ls dicom_series | grep '76mm' `)
scanidx=0


if [[ ${#surfscans[*]} -ne 12 ]]; then
    echo "Not enough surf coil images"
    exit 1
fi 

if [ ! -d nifti ]; then
    mkdir nifti
fi

if [ -f nifti/mag[1-3].nii.gz ];then
    echo 'mag files exist. delete before continuing'
    exit 1
fi

for i in `seq 1 3`; do
    echo "Scan " $i " , mag idx " $scanidx; 

    mrconvert dicom_series/${surfscans[$scanidx]} nifti/mag$i.nii.gz -datatype float32
    ((++scanidx))
    echo "pha idx " $scanidx
    mrconvert dicom_series/${surfscans[$scanidx]} nifti/pha$i.nii.gz -datatype float32
    ((++scanidx))
    ((++scanidx))
    echo "swi idx " $scanidx
    mrconvert dicom_series/${surfscans[$scanidx]} nifti/swi$i.nii.gz -datatype float32
    ((++scanidx))
    # echo $scanidx

#    matlab -nodesktop -nosplash -r "addpath('/Volumes/AmandaProjects/Projects/MRH002_BionicVision/scripts/');cplxfilter('nifti/mag"$i".nii.gz','nifti/pha"$i".nii.gz','nifti/abs"$i".nii.gz');quit"
done



echo "Check nifti images. Press enter to continue"
read

echo " Running make_mask"
matlab -nodesktop -nosplash -r "addpath('/Volumes/AmandaProjects/Projects/MRH002_BionicVision/scripts/');make_mask('nifti/mag1.nii.gz');make_mask('nifti/mag2.nii.gz');make_mask('nifti/mag3.nii.gz');quit"


echo " Running ants"
cd ants
ants 3 -m CC[mag1_for_reg.nii.gz,mag2_for_reg.nii.gz,1,2] -o 2to1 -i 0 --rigid-affine true
ants 3 -m CC[mag1_for_reg.nii.gz,mag3_for_reg.nii.gz,1,2] -o 3to1 -i 0 --rigid-affine true
cd ..





average_surfcoil(){
    
if [ $# -lt 3 ]; then
    echo "average_surfcoil.sh must have three, four  or six arguments: "
    echo "average_surfcoil.sh img1 img2 img3 output_path 2to1affine 3to1affine"
    
    exit 1
fi

img1=$1
img2=$2 
img3=$3
imgname=`echo $img1 | sed 's/.*\/\([a-z]*\).*/\1/'`
if [ $# -eq 3 ]; then
    opath="nifti/"
else
    opath=$4
fi

if [ $# -eq 6 ]; then
    affine21=$5
    affine31=$6
else
    affine21="ants/2to1Affine.txt"    
    affine31="ants/3to1Affine.txt"
fi

if [ ! -f $affine21 ]; then
    echo "could not find 2to1Affine.txt"
    exit 1
fi  
 rm -f $opath/${imgname}*_dbl_res.nii.gz $opath/avg_${imgname}*

 mrtransform $img1 -template $img1 -upsample 2 $opath/${imgname}1_dbl_res.nii.gz
 mrtransform $img2 -template $img2 -upsample 2 $opath/${imgname}2_dbl_res.nii.gz
 mrtransform $img3 -template $img3 -upsample 2 $opath/${imgname}3_dbl_res.nii.gz


    WarpImageMultiTransform 3 $opath/${imgname}2_dbl_res.nii.gz $opath/${imgname}2_in_img1_dbl_res.nii.gz -R $opath/${imgname}1_dbl_res.nii.gz --use-BSpline $affine21
    WarpImageMultiTransform 3 $opath/${imgname}3_dbl_res.nii.gz $opath/${imgname}3_in_img1_dbl_res.nii.gz -R $opath/${imgname}1_dbl_res.nii.gz --use-BSpline $affine31

fslmaths $opath/${imgname}1_dbl_res -add $opath/${imgname}2_in_img1_dbl_res -add $opath/${imgname}3_in_img1_dbl_res -div 3 $opath/avg_${imgname}
rm -f $opath/${imgname}*_dbl_res.nii.gz



}


# echo " Running averaging script"
# /Volumes/AmandaProjects/Projects/MRH002_BionicVision/scripts/average_surfcoil.sh nifti/mag1.nii.gz nifti/mag2.nii.gz nifti/mag3.nii.gz nifti/ ants/2to1Affine.txt ants/3to1Affine.txt
# /Volumes/AmandaProjects/Projects/MRH002_BionicVision/scripts/average_surfcoil.sh nifti/pha1.nii.gz nifti/pha2.nii.gz nifti/pha3.nii.gz nifti/ ants/2to1Affine.txt ants/3to1Affine.txt
# /Volumes/AmandaProjects/Projects/MRH002_BionicVision/scripts/average_surfcoil.sh nifti/swi1.nii.gz nifti/swi2.nii.gz nifti/swi3.nii.gz nifti/ ants/2to1Affine.txt ants/3to1Affine.txt 
# # /Volumes/AmandaProjects/Projects/MRH002_BionicVision/scripts/average_surfcoil.sh nifti/abs1.nii.gz nifti/abs2.nii.gz nifti/abs3.nii.gz nifti/ ants/2to1Affine.txt ants/3to1Affine.txt
# echo "Done."


echo " Running averaging script"
average_surfcoil( 'nifti/mag1.nii.gz', 'nifti/mag2.nii.gz', 'nifti/mag3.nii.gz')
average_surfcoil( 'nifti/pha1.nii.gz', 'nifti/pha2.nii.gz', 'nifti/pha3.nii.gz') 
average_surfcoil( 'nifti/swi1.nii.gz', 'nifti/swi2.nii.gz', 'nifti/swi3.nii.gz') 