function out = normalise(in,mask,clim)
% NORMALISE normalises all elements in an array to the range [0,1]
%
% OUT = NORMALISE(IN, MASK, CLIM)
%
% The MASK parameter is optional. It should be an array the same size as
% IN. All non-zero values indicate foreground. All foreground elements are
% normalised to the range [0,1]. All background elements are set to zero.
% If CLIM is given, values are truncated to the range CLIM.
 
% Created 31 March 2009 by Amanda Ng
% Modified 16 April 2010 by Amanda Ng
%   - added mask parameter.
% Modified 23 April 2010 by Amanda Ng
%   - added clim parameter

if nargin == 0
    error 'Incorrect number of parameters supplied'
elseif nargin < 2 || isempty(mask)
    mask = ones(size(in));
end

mask = (mask~=0);

if nargin < 3
    clim = interval(in(mask));
end

in(in < clim(1)) = clim(1);
in(in > clim(2)) = clim(2);

out = double((in - clim(1))./range(clim)).*mask;

out(out < 0) = 0;
out(out > 1) = 1;

% original code
%out = (in - min(in(mask(:))))./range(in(mask(:))).*mask;