FLIRT=flirt -verbose 5 -noclamp

#.PHONY : convert
#convert :
        mrconvert 10_ep2d_diff_b2000_64_2mm\ iso\ 1 diff.mif
        mrconvert 3_t1_mp2rage_sag_p2_iso_scanner_standard_t1_mp2rage_sag_p2_iso_scanner_standard_INV1 mp2rage_inv.mif


#diff.nii: diff.mif
        mrconvert diff.mif diff.nii

#mp2rage_inv.nii: mp2rage_inv.mif
        mrconvert mp2rage_inv.mif mp2rage_inv.nii


#register: diff.nii mp2rage_inv.nii
        time ${FLIRT} \
                -in mp2rage_inv.nii \
                -ref diff.nii \
                -out mp2rage_reg.nii -omat mp2rage_reg.mat \
                -bins 256 -cost normmi -searchrx -5 5 -searchry -5 5 \
                -searchrz -5 5 -dof 6 -interp trilinear
        mrconvert mp2rage_reg.nii.gz mp2rage_reg.mif


#run: convert

        average diff.mif -axis 3 - | threshold - - | median3D - - | median3D - mask_DWI.mif
        dwi2tensor diff.mif dt_DWI.mif
        tensor2FA dt_DWI.mif    - | mrmult - mask_DWI.mif fa_DWI.mif
        tensor2vector dt_DWI.mif  - | mrmult - fa_DWI.mif ev_DWI.mif
        erode mask_DWI.mif -npass 3 - | mrmult fa_DWI.mif - - | threshold - -abs 0.7 sf_DWI.mif
        streamtrack DT_STREAM diff.mif -seed mask_DWI.mif -mask mask_DWI.mif DWI_whole_brain.tck -num 10000
	
	mrview mp2rage_reg.mif diff.mif
