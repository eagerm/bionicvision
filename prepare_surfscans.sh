#!/usr/bin/env bash
# ls dicom_series/ | grep 0.76.*Mag_Images
# ls dicom_series/ | grep 0.76.*Pha_Images
# ls dicom_series/ | grep 0.76.*SWI_Images
declare -a surfscans=(`ls dicom_series | grep '76mm' `)
scanidx=0


if [[ ${#surfscans[*]} -ne 12 ]]; then
    echo "Not enough surf coil images"
    exit 1
fi 

if [ ! -d nifti ]; then
    mkdir nifti
fi

if [ -f nifti/mag[1-3].nii.gz ];then
    echo 'mag files exist. delete before continuing'
    exit 1
fi

for i in `seq 1 3`; do
    echo "Scan " $i " , mag idx " $scanidx; 

    mrconvert dicom_series/${surfscans[$scanidx]} nifti/mag$i.nii.gz -datatype float32
    ((++scanidx))
    echo "pha idx " $scanidx
    mrconvert dicom_series/${surfscans[$scanidx]} nifti/pha$i.nii.gz -datatype float32
    ((++scanidx))
    ((++scanidx))
    echo "swi idx " $scanidx
    mrconvert dicom_series/${surfscans[$scanidx]} nifti/swi$i.nii.gz -datatype float32
    ((++scanidx))
    # echo $scanidx

#    matlab -nodesktop -nosplash -r "addpath('/Volumes/AmandaProjects/Projects/MRH002_BionicVision/scripts/');cplxfilter('nifti/mag"$i".nii.gz','nifti/pha"$i".nii.gz','nifti/abs"$i".nii.gz');quit"
done



#fslhd nifti/mag3.nii.gz | grep '^dim3'| awk '{print $2}'
unset dim3
declare -a dim3=(`ls nifti/mag[1-3].nii.gz |xargs -n1 fslhd | grep '^dim3'| awk '{print $2}'`)
if [ ${dim3[0]} -ne ${dim3[1]} -o ${dim3[0]} -ne ${dim3[2]} -o ${dim3[1]} -ne ${dim3[2]} ]; then
    echo " Nifti mag files not the same size ", ${dim3[0]}, ${dim3[1]}, ${dim3[2]}
    exit 1
fi
unset dim3
declare -a dim3=(`ls nifti/pha[1-3].nii.gz |xargs -n1 fslhd | grep '^dim3'| awk '{print $2}'`)
if [ ${dim3[0]} -ne ${dim3[1]} -o ${dim3[0]} -ne ${dim3[2]} -o ${dim3[1]} -ne ${dim3[2]} ]; then
    echo " Nifti phase files not the same size ", ${dim3[0]}, ${dim3[1]}, ${dim3[2]} 
    exit 1
fi
unset dim3
declare -a dim3=(`ls nifti/swi[1-3].nii.gz |xargs -n1 fslhd | grep '^dim3'| awk '{print $2}'`)
if [ ${dim3[0]} -ne ${dim3[1]} -o ${dim3[0]} -ne ${dim3[2]} -o ${dim3[1]} -ne ${dim3[2]} ]; then
    echo " Nifti swi files not the same size ", ${dim3[0]}, ${dim3[1]}, ${dim3[2]} 
    exit 1
fi

