# README #

Copyright 2014 Michael Eager (michael.eager@monash.edu).

The BionicVision is a series of shell and MATLAB scripts for an image processing procedure that is part of a Monash Biomedical Imaging and Monash Vision Group project.  

The BionicVision package is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

The BionicVision_scripts package is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with the Bionic Vision source code. If not, see http://www.gnu.org/licenses/.


### Quick summary ###

The objective of the scripts is to automate the averaging three surface coil SWI scans.

Dicom files should be placed in numerically consecutive folders within the base folder dicom_series. This must include three each of *Mag_images, *Phase_Images, *mIP_IMages(SW) [unused but required], and *SWI_Images.

The pipeline is as follows:

* Convert dicoms in 'dicom_series' to nifti save in 'nifti' (a regexp searches for swi3d and 0.76mm)
* Double the resolution of the nifti files
* Register mag2 and mag3 to mag1 and save affine files to affine21.txt and affine31.txt
* Transform Mag, Phase and SWI files to mag1 space
* Average Mag, Phase and SWI 



* Version
* Current bugs and faults

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* How to run tests
* Deployment instructions


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Michael Eager (michael.eager@monash.edu) or someone in the Imaging Team at MBI