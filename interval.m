function out = interval(in)
% INTERVAL returns the minimum and maximum of all elements in an array
%
% OUT = INTERVAL(IN)

% created 19 April 2010 by Amanda Ng

out = [min(in(:)) max(in(:))];