#!/usr/bin/env bash


if [ $# -lt 4 ]; then
    echo "average_surfcoil.sh must have four  or six arguments: "
    echo "average_surfcoil.sh img1 img2 img3 output_path 2to1affine 3to1affine"
    
    exit 1
fi

img1=$1
img2=$2 
img3=$3

opath=$4

imgname=`echo $img1 | sed 's/.*\/\([a-z]*\).*/\1/'`
if [ $# -eq 6 ]; then
    affine21=$5
    affine31=$6
else
    affine21="../ants/2to1Affine.txt"    
    affine31="../ants/3to1Affine.txt"
fi

if [ ! -f $affine21 ]; then
    echo "could not find 2to1Affine.txt"
    exit 1
fi  


if [ ! -d $opath ]; then
    echo "could not find output dir $opath"
    exit 1
fi  


# if [ $img1 -nt $opath/${imgname}1_dbl_res.nii.gz ] ; then

 rm -f $opath/${imgname}*_dbl_res.nii.gz $opath/avg_${imgname}*

 mrtransform $img1 -template $img1 -upsample 2 $opath/${imgname}1_dbl_res.nii.gz
 mrtransform $img2 -template $img2 -upsample 2 $opath/${imgname}2_dbl_res.nii.gz
 mrtransform $img3 -template $img3 -upsample 2 $opath/${imgname}3_dbl_res.nii.gz
# fi

# if  [ [ ! -f $opath/${imgname}2_in_img1_dbl_res.nii.gz ] || [ [ -f $opath/${imgname}2_in_img1_dbl_res.nii.gz ] && [ $opath/${imgname}2_dbl_res.nii.gz -nt $opath/${imgname}2_in_img1_dbl_res.nii.gz ] ]; then
    WarpImageMultiTransform 3 $opath/${imgname}2_dbl_res.nii.gz $opath/${imgname}2_in_img1_dbl_res.nii.gz -R $opath/${imgname}1_dbl_res.nii.gz --use-BSpline $affine21
#fi
#if [ [ ! -f $opath/${imgname}3_in_img1_dbl_res.nii.gz ] || [ [ -f $opath/${imgname}3_in_img1_dbl_res.nii.gz ] &&  $opath/${imgname}3_dbl_res.nii.gz -nt $opath/${imgname}3_in_img1_dbl_res.nii.gz ] ]; then
    WarpImageMultiTransform 3 $opath/${imgname}3_dbl_res.nii.gz $opath/${imgname}3_in_img1_dbl_res.nii.gz -R $opath/${imgname}1_dbl_res.nii.gz --use-BSpline $affine31
#fi

fslmaths $opath/${imgname}1_dbl_res -add $opath/${imgname}2_in_img1_dbl_res -add $opath/${imgname}3_in_img1_dbl_res -div 3 $opath/avg_${imgname}
rm -f $opath/${imgname}*_dbl_res.nii.gz

