#!/usr/bin/env bash

if test ${MASSIVEUSERNAME+defined};then
  module load ants mrtrix/0.2.12 fsl/5.0.6
fi
# make -f $(dirname $0)/Makefile check
export PATH=$(dirname $0):$PATH
make -f $(dirname $0)/Makefile nifti/avg_mag.nii.gz nifti/avg_pha.nii.gz nifti/avg_swi.nii.gz
