
echo " T1 registration and transformation"

mrconvert dicom_series/[1-9]_t1_mprage_sag_p2_iso_1mm nifti/T1.nii.gz -datatype float32



bet nifti/T1 nifti/T1Brain -R -m -n 
fslmaths nifti/T1Brain_mask.nii.gz -dilM nifti/tmp.nii.gz
fslmaths nifti/T1 -mas nifti/T1Brain_mask.nii.gz nifti/T1Brain



matlab -nodesktop -nosplash -singleCompThread -r "tic;orignii = load_untouch_nii('nifti/T1Brain.nii.gz'); toc;t1 = orignii.img; minX = 25; maxX = 175;minY = 20; maxY = 100;minZ = 100; maxZ = 200;tmp = t1(minX:maxX,minY:maxY,minZ:maxZ);nii = orignii;hdr = orignii.hdr;nii.img = tmp;toc;nii.hdr.dime.dim(2:4) = size(nii.img);affine = cat(1, hdr.hist.srow_x, hdr.hist.srow_y, hdr.hist.srow_z);newtranslation = affine*[minX-1; minY-1; minZ-1; 1];nii.hdr.hist.qoffset_x = newtranslation(1);nii.hdr.hist.qoffset_y = newtranslation(2);nii.hdr.hist.qoffset_z = newtranslation(3);nii.hdr.hist.srow_x(4) = newtranslation(1);nii.hdr.hist.srow_y(4) = newtranslation(2);nii.hdr.hist.srow_z(4) = newtranslation(3);toc;save_untouch_nii(nii, 'ants/t1_for_reg.nii.gz');toc;quit"



cd ants
ants 3 -m MI[t1_for_reg.nii.gz,mag1_for_reg.nii.gz,1,2] -o 1toT1 -i 0 --rigid-affine true
mrtransform t1_for_reg.nii.gz t1_for_reg_dbl_res.nii.gz -template t1_for_reg.nii.gz -upsample 2
cd ..

