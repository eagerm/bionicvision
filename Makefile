
# DIR = $(dirname $0)
BINDIR=$(shell dirname $(shell which run_makefile.sh))
#/Volumes/AmandaProjects/Projects/MRH002_BionicVision/scripts/
WHICH=/usr/bin/which
opath = nifti
affine21 = ants/2to1Affine.txt
affine31 = ants/3to1Affine.txt

FILES = nifti/mag1.nii.gz nifti/mag2.nii.gz nifti/mag3.nii.gz nifti/pha1.nii.gz nifti/pha2.nii.gz nifti/pha3.nii.gz nifti/swi1.nii.gz nifti/swi2.nii.gz nifti/swi3.nii.gz
DBL_RES_FILES = $(patsubst %.nii.gz,%_dbl_res.nii.gz,$(FILES))

#LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/usr/local/gmp/4.3.1/lib:/usr/local/gtk+/gtk+dependencies/glib/lib:/usr/local/gtkglext/1.2.0/lib:/usr/local/gtk+/gtk+dependencies/libxml/2.7.1/lib:/usr/local/gtk+/gtk+dependencies/fontconfig/2.8.0/lib:/usr/local/gtk+/2.18.0/lib:/usr/local/mrtrix/mrtrix_dependencies/gtkmm/2.14.5/lib:/usr/local/mrtrix/mrtrix_dependencies/glibmm/2.24.2/lib:/usr/local/gtk+/gtk+dependencies/cairo/1.10.2/lib:/usr/local/gtk+/gtk+dependencies/pango/1.28.4/lib:/usr/local/mrtrix/mrtrix_dependencies/pangomm/2.24.0/lib/::/usr/local/mrtrix/mrtrix_dependencies/cairomm/1.9.8/lib:/usr/local/mrtrix/mrtrix_dependencies/gtkmm/2.14.5/lib:/usr/local/mrtrix/mrtrix_dependencies/glibmm/2.24.2/lib:/usr/local/mrtrix/mrtrix_dependencies/sigc++/2.2.8/lib:/gpfs/M2Home/eagerm/Monash016/eagerm/mrtrix-0.2.12/lib" 
#PATH="~/Monash016/eagerm/mrtrix-0.2.12/lib:~/Monash016/eagerm/mrtrix-0.2.12/bin:/usr/local/gtk+/gtk+dependencies/glib/bin:/usr/local/gtk+/2.16.4/bin:/usr/local/mrtrix/mrtrix_dependencies/gtkmm/3.0.1/bin:$PATH"

DEPENDENCIES= matlab ants fslmaths WarpImageMultiTransform mrtransform 	$(BINDIR)/prepare_surfscans.sh 	$(BINDIR)/average_surfcoil.sh




.PHONY: check
check:
	for req in $(DEPENDENCIES); do \
	    $(WHICH) $$req > /dev/null || echo "MISSING DEPENDENCY $$req"; \
	done


all:  $(opath)/avg_mag.nii.gz $(opath)/avg_pha.nii.gz $(opath)/avg_swi.nii.gz



$(FILES):  #nifti/[mps][ahw][gai][123].nii.gz:
	$(BINDIR)/prepare_surfscans.sh

.PHONY: NIFTI
NIFTI: 
	-rm -rf NIFTI
	curl http://www.mathworks.com.au/matlabcentral/fileexchange/8797-tools-for-nifti-and-analyze-image?download=true | sed 's#<html><body>You are being <a href="\(.*\)">redirected</a>.</body></html>#\1#' | xargs wget
	mv download NIFTI.zip
	-mkdir NIFTI
	(cd NIFTI && unzip ../NIFTI.zip)



nifti/abs1.nii.gz nifti/abs2.nii.gz nifti/abs3.nii.gz: $(FILES)
	matlab -nodesktop -nosplash -r "addpath(genpath('$(BINDIR)'));cplxfilter('nifti/mag1.nii.gz','nifti/pha1.nii.gz','nifti/abs1.nii.gz');cplxfilter('nifti/mag2.nii.gz','nifti/pha2.nii.gz','nifti/abs2.nii.gz');cplxfilter('nifti/mag3.nii.gz','nifti/pha3.nii.gz','nifti/abs3.nii.gz');quit"


ants/mag1_for_reg.nii.gz ants/mag2_for_reg.nii.gz ants/mag3_for_reg.nii.gz: ants/mag%_for_reg.nii.gz: nifti/mag%.nii.gz
	-[ ! -d ants ] && mkdir ants
	matlab -nodesktop -nosplash -r "addpath(genpath('$(BINDIR)'));make_mask('nifti/mag$*.nii.gz');quit"


$(affine21): ants/mag1_for_reg.nii.gz ants/mag2_for_reg.nii.gz
	(cd ants;ants 3 -m CC[mag1_for_reg.nii.gz,mag2_for_reg.nii.gz,1,2] -o 2to1 -i 0 --rigid-affine true)

$(affine31): ants/mag1_for_reg.nii.gz ants/mag3_for_reg.nii.gz
	(cd ants;ants 3 -m CC[mag1_for_reg.nii.gz,mag3_for_reg.nii.gz,1,2] -o 3to1 -i 0 --rigid-affine true)

$(DBL_RES_FILES):  nifti/%_dbl_res.nii.gz: nifti/%.nii.gz
	mrtransform $< -template $< -upsample 2 $@


#nifti/[mps][ahw][gai]2_in_img1_dbl_res.nii.gz: nifti/[mps][ahw][gai]1_dbl_res.nii.gz nifti/[mps][ahw][gai]2_dbl_res.nii.gz $affine21
#    WarpImageMultiTransform 3 nifti/${imgname}2_dbl_res.nii.gz nifti/${imgname}2_in_img1_dbl_res.nii.gz -R nifti/${imgname}1_dbl_res.nii.gz --use-BSpline $affine21



$(patsubst %2.nii.gz,%2_in_img1_dbl_res.nii.gz,$(filter %2.nii.gz,$(FILES))): nifti/%2_in_img1_dbl_res.nii.gz: nifti/%1_dbl_res.nii.gz nifti/%2_dbl_res.nii.gz $(affine21)
	WarpImageMultiTransform 3 nifti/$*2_dbl_res.nii.gz $@ -R nifti/$*1_dbl_res.nii.gz --use-BSpline $(affine21)

$(patsubst %3.nii.gz,%3_in_img1_dbl_res.nii.gz, $(filter %3.nii.gz,$(FILES))): nifti/%3_in_img1_dbl_res.nii.gz: nifti/%1_dbl_res.nii.gz nifti/%3_dbl_res.nii.gz $(affine31)
	WarpImageMultiTransform 3 nifti/$*3_dbl_res.nii.gz $@ -R nifti/$*1_dbl_res.nii.gz --use-BSpline $(affine31)


#nifti/${imgname}3_in_img1_dbl_res.nii.gz:  nifti/${imgname}1_dbl_res.nii.gz  nifti/${imgname}3_dbl_res.nii.gz $affine31
#    WarpImageMultiTransform 3 nifti/${imgname}3_dbl_res.nii.gz nifti/${imgname}3_in_img1_dbl_res.nii.gz -R nifti/${imgname}1_dbl_res.nii.gz --use-BSpline $affine31

nifti/avg_mag.nii.gz nifti/avg_pha.nii.gz nifti/avg_swi.nii.gz: nifti/avg_%.nii.gz: nifti/%1_dbl_res.nii.gz  nifti/%2_in_img1_dbl_res.nii.gz nifti/%3_in_img1_dbl_res.nii.gz 
	fslmaths nifti/$*1_dbl_res -add nifti/$*2_in_img1_dbl_res -add nifti/$*3_in_img1_dbl_res -div 3 $@
#	

.PHONY: clean-dbl
clean-dbl:
	-rm -f nifti/*dbl_res.nii.gz

.PHONY: clean-nii
clean-nii:
	-rm -f nifti/mag[1-3].nii.gz
	-rm -f nifti/pha[1-3].nii.gz
	-rm -r nifti/swi[1-3].nii.gz

.PHONY: clean-all
clean-all : clean-dbl clean-nii

.PHONY: full-clean
full-clean: clean-dbl clean-nii
	-rm -rf ants
	-rm -rf nifti

.PHONY : clean
clean :
	-for files in $(FILES); do test -f $$file && rm -f $$file; done
	-rm -f ants/mag1_for_reg.nii.gz ants/mag2_for_reg.nii.gz ants/mag3_for_reg.nii.gz
	-rm -f nifti/*dbl_res.nii.gz

#nifti/avg_mag.nii.gz: nifti/mag1_dbl_res.nii.gz  nifti/mag2_in_img1_dbl_res.nii.gz nifti/mag3_in_img1_dbl_res.nii.gz # nifti/[mps][ahw][gai]2_in_img1_dbl_res.nii.gz nifti/[mps][ahw][gai]3_in_img1_dbl_res.nii.gz
#	imgname=$(patsubst nifti/avg_%.nii.gz, %, $@)
#	fslmaths nifti/$(imgname)1_dbl_res -add nifti/$(imgname)2_in_img1_dbl_res -add nifti/$(imgname)3_in_img1_dbl_res -div 3 $@
